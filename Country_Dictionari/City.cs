﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Country_Dictionari
{
    class City : Dictionary<string,Street>
    {
        public string name;

        public void Add(Street item)
        {
            if (!ContainsKey(item.name))
                base.Add(item.name, item);
        }
    }
}
