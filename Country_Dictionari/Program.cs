﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Country_Dictionari
{
    class Program
    {
        static void Main(string[] args)
        {
            var city = new City { name = "Yerevan" };

            var street1 = new Street { name = "Galshoyan", cityName = "Yerevan" };
            var street2 = new Street { name = "Mikoyan", cityName = "Yerevan" };


            street1.Add(new Building { number = 1, streetname = "Galshoyan" });
            street1.Add(new Building { number = 3, streetname = "Galshoyan" });
            street1.Add(new Building { number = 5, streetname = "Galshoyan" });
            street1.Add(new Building { number = 7, streetname = "Galshoyan" });
            street1.Add(new Building { number = 9, streetname = "Galshoyan" });
            street1.Add(new Building { number = 11, streetname = "Galshoyan" });
            street1.Add(new Building { number = 13, streetname = "Galshoyan" });

            street2.Add(new Building { number = 2, streetname = "Mikoyan" });
            street2.Add(new Building { number = 4, streetname = "Mikoyan" });
            street2.Add(new Building { number = 6, streetname = "Mikoyan" });
            street2.Add(new Building { number = 8, streetname = "Mikoyan" });
            street2.Add(new Building { number = 10, streetname = "Mikoyan" });
            street2.Add(new Building { number = 12, streetname = "Mikoyan" });
            street2.Add(new Building { number = 14, streetname = "Mikoyan" });

            city.Add(street1);
            city.Add(street2);


        }
    }
}
