﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Country_Dictionari
{
    class Street : Dictionary<int, Building>
    {
        public string name;
        public string cityName;

        public void Add(Building item)
        {
            if (!ContainsKey(item.number))
                base.Add(item.number, item);
        }
    }
}
